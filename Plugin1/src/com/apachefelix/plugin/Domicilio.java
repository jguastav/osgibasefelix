package com.apachefelix.plugin;

public class Domicilio {

	private String calle;
	private String barrio;
	private String numcasa;
	private String avenida;
	
	public Domicilio(){
		
	}
	
	public Domicilio(String calle, String barrio, String numcasa, String avenida) {
		super();
		this.calle = calle;
		this.barrio = barrio;
		this.numcasa = numcasa;
		this.avenida = avenida;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getNumcasa() {
		return numcasa;
	}

	public void setNumcasa(String numcasa) {
		this.numcasa = numcasa;
	}

	public String getAvenida() {
		return avenida;
	}

	public void setAvenida(String avenida) {
		this.avenida = avenida;
	}
}
