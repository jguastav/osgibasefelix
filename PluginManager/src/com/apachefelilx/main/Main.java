package com.apachefelilx.main;

import java.util.HashMap; 
import java.util.Map; 
import org.apache.felix.framework.Felix; 
import org.osgi.framework.*; 
 
public class Main { 
  static Felix m_framework; 
  
  //url de los plugins
  private static final String classpath ="/Users/sponce/Documents/workspace/apachefelix/";
  
  public static void main(String[] args) throws Exception { 
    try { 
 
      final Map configMap = new HashMap(); 
      configMap.put(Constants.FRAMEWORK_STORAGE_CLEAN, "onFirstInit"); 
      m_framework = new Felix(configMap); 
      m_framework.init(); 
 
      final BundleContext context = m_framework.getBundleContext(); 
 
      /*Bundle provider = context.installBundle("file:bundles/provider-3.0.jar"); 
      Bundle consumer = context.installBundle("file:bundles/consumer-3.0.jar"); */
      
      Bundle plugin1 = context.installBundle("file:"+ classpath +"bundles/plugin1.jar"); 
      Bundle plugin2 = context.installBundle("file:"+ classpath +"bundles/plugin2.jar"); 
      
      m_framework.start(); 
 
      plugin1.start(); 
      plugin2.start(); 
      plugin1.stop(); 
      plugin2.stop();
      
      
      m_framework.stop(); 
 
    } catch (Exception ex) { 
      System.err.println("Error starting program: " + ex); 
      ex.printStackTrace(); 
      System.exit(0); 
    } 
  } 
}
