package com.apachefelix.plugin;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceListener;
import org.osgi.framework.ServiceEvent;
 
public class Activator implements BundleActivator, ServiceListener {
	
	Cliente cliente;
	
    public void start(BundleContext context) {
    	init();
    	System.out.println("Arrancando plugin 2! ");
        System.out.println(imprimirDomicilio(cliente));
        context.addServiceListener(this);
        }
 
    public void stop(BundleContext context) {
        context.removeServiceListener(this);
        System.out.println("Deteniendo plugin 2!");
        }
 
    public void serviceChanged(ServiceEvent event) {
        String[] objectClass = (String[])
            event.getServiceReference().getProperty("objectClass");
 
        if (event.getType() == ServiceEvent.REGISTERED)
        {
            System.out.println(
                "Ex1: Service of type " + objectClass[0] + " registered.");
        }
        else if (event.getType() == ServiceEvent.UNREGISTERING)
        {
            System.out.println(
                "Ex1: Service of type " + objectClass[0] + " unregistered.");
        }
        else if (event.getType() == ServiceEvent.MODIFIED)
        {
            System.out.println(
                "Ex1: Service of type " + objectClass[0] + " modified.");
        }
    }
    
    public String imprimirDomicilio(Cliente cliente){
    	StringBuilder sb = new StringBuilder();
    	sb.append("La direccion del Cliente 2 " + cliente.getNombre() + " " + cliente.getApellido());
    	sb.append("es : \n " + cliente.getDomicilio().getAvenida() +", ");
    	sb.append(cliente.getDomicilio().getCalle());    	
    	return  sb.toString();
    }
    
    public void init(){
    	cliente = new Cliente();
		cliente.setNombre("Jose");
		cliente.setApellido("Perez");
		cliente.setDni("99999999");
		cliente.setDomicilio(new Domicilio("Calle los pinitos","Barrio 1","Casa 4","Avenida La Union"));
	}
}
